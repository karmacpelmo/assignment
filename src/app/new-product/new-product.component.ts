import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  newProduct = [
    {
      image: "assets/images/41.jpg",
      name: 'Mauris facilisis eros',
      price: 28333,
      sale: false
    },
    {
      image: "assets/images/42.jpg",
      name: 'Nullam tortor risus',
      price: 28326,
      sale: false
    },
    {
      image: "assets/images/43.jpg",
      name: 'Aenean id felis quis',
      price: 31827,
      sale: true,
      newprice: 400
    }
  ]

}
