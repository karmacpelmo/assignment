import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-featured-products',
  templateUrl: './featured-products.component.html',
  styleUrls: ['./featured-products.component.scss']
})
export class FeaturedProductsComponent implements OnInit {

  constructor() { 
  }

  featuredProduct = [
    {
      image: "assets/images/37.jpg",
      name: 'Mauris facilisis eros',
      price: 28333,
      rating: 3
    },
    {
      image: "assets/images/34.jpg",
      name: 'Nullam tortor risus',
      price: 28326,
      rating: 5
    },
    {
      image: "assets/images/35.jpg",
      name: 'Aenean id felis quis',
      price: 31827,
      rating: 4
    },
    {
      image: "assets/images/36.jpg",
      name: 'Cool & Comfy Onlala',
      price: 10000,
      rating: 4
    }

]

  ngOnInit() {
  }

  


}
