import { NgModule } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule, 
  MatSelectModule, 
  MatMenuModule, 
  MatDividerModule, 
  MatButtonToggleModule,
   MatIconModule, 
   MatListModule,
  MatLineModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatRippleModule
} from '@angular/material';


const Material =[
  MatButtonModule,
  MatSelectModule,
  MatMenuModule,
  MatDividerModule,
  MatButtonToggleModule,
  MatIconModule,
  MatListModule,
  MatLineModule,
  MatFormFieldModule,
  MatInputModule,
  MatCardModule,
  MatBadgeModule,
  MatRippleModule,
  MatRadioModule
];


@NgModule({
  imports: [ Material],
  exports: [Material ]
})
export class MaterialModule { }
